Regular Languages
=================

.. toctree::
   :maxdepth: 3

.. module:: maquinas.regular

Deterministic Finite Automaton
------------------------------

.. autoclass:: maquinas.regular.dfa.DeterministicFiniteAutomaton
   :members:
   :inherited-members:

Non Deterministic Finite Automaton
----------------------------------

.. autoclass:: maquinas.regular.ndfa.NonDeterministicFiniteAutomaton
   :members:
   :inherited-members:

Non Deterministic Finite Automaton with epsilon
-----------------------------------------------

.. autoclass:: maquinas.regular.ndfa_e.NonDeterministicFiniteAutomaton_epsilon
   :members:
   :inherited-members:

Regular Grammar
---------------

.. autoclass:: maquinas.regular.rg.RegularGrammar
   :members:
   :inherited-members:

Reductions
----------

.. automodule:: maquinas.regular.reductions
   :members:
   :inherited-members:

Minimization
------------

.. automodule:: maquinas.regular.minimization
   :members:
   :inherited-members:
