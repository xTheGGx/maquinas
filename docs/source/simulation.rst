Simulation
===========

.. toctree::
   :maxdepth: 2

.. module:: maquinas.simulation

Simulation control
--------------------

.. autoclass:: maquinas.simulation.Simulation
   :members:
   :inherited-members:

