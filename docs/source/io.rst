
Input/output
============

.. toctree::
   :maxdepth: 2

.. module:: maquinas.io

The io module contains...

.. currentmodule:: maquinas.io


load_fa
--------------------

.. autofunction:: maquinas.io.load_fa

load_jflap
----------

.. autofunction:: maquinas.io.load_jflap
