Parsing and visualazing trees
=============================

.. toctree::
   :maxdepth: 2


Parsing a string
----------------

To parse a string use :func:`~maquinas.contextfree.cfg.ContextFreeGrammar.parse`

.. code-block:: python

    from maquinas.contextfree.cfg import ContextFreeGrammar as CFG
    g=CFG("S-> ACB; C-> ACB; C -> AB; A -> a; B->b")
    roots,chart,forest=g.parse("aabcc")

The function returns tree elements _roots_ symbols, the _chart_ structure and the _forest_ parse

To verify is the string was part of the language grammar check the roots

.. code-block:: python

    print(f'Accepts?', "yes" if roots else "no" )

To print the chart use :func:`~maquinas.contextfree.cfg.ContextFreeGrammar.print_chart`

.. code-block:: python

    g.print_chart(s,chart,pointers=True)


To extract the trees from the forest use :func:`~maquinas.contextfree.cfg.ContextFreeGrammar.extract_trees`

.. code-block:: python

    trees=g.extract_trees(forest)

Saving trees images
-------------------

To save trees into an image file you can use  :func:`~maquinas.contextfree.cfg.ContextFreeGrammar.save_trees_img`

.. code-block:: python

    g.save_trees_img(trees,filename=filename)


Visualizing in Notebooks
------------------------

To display the forest grpah use :func:`~maquinas.contextfree.cfg.ContextFreeGrammar.graph_forest`

.. code-block:: python

    m.graph_forest(forest)


To display trees use :func:`~maquinas.contextfree.cfg.ContextFreeGrammar.graph_trees`

.. code-block:: python

    m.graph_trees(trees)


To display a tree use :func:`~maquinas.contextfree.cfg.ContextFreeGrammar.graph_tree`

    m.graph_tree(trees[0])


