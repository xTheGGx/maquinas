Alphabets and Languages
=======================

.. toctree::
   :maxdepth: 2

.. module:: maquinas.languages

Alphabet
--------

.. autoclass:: maquinas.languages.Alphabet
   :members:
   :inherited-members:


Mapping
--------

.. autoclass:: maquinas.languages.Mapping
   :members:
   :inherited-members:

Language
--------

.. autoclass:: maquinas.languages.Language
   :members:
   :inherited-members:

