.. maquinas documentation master file, created by
   sphinx-quickstart on Sun Nov  8 20:42:14 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to maquinas's documentation!
====================================

maquinas is a library to build computing machines and grammars. It can be used as a support for the teaching Formal Language Theory.

Contents
--------

.. toctree::
   :maxdepth: 2

   installation
   quickstart


Reference
---------

.. toctree::
   :maxdepth: 2

   al
   rl
   cfl
   rel
   simulation
   collections
   io

Changes
-------

Changelog

.. toctree::
   :maxdepth: 2

   changelog

