Collections
===========

.. toctree::
   :maxdepth: 2

.. module:: maquinas.collections

Exmaples
--------

.. autoclass:: maquinas.collections.Examples
   :members:
   :inherited-members:

Collections
-----------

.. autodata:: maquinas.collections.jflap
