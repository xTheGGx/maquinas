from nose.tools import *

from maquinas.exceptions import *
from maquinas.recursivelyenumerable.tspda import TwoStackPushDownAutomaton
m=TwoStackPushDownAutomaton(
    Q=['q_0','q_1','q_2','q_3'],
     sigma=['a','b'],
     gamma=['A'],
     q_0='q_0',
     A=['q_3'],
     delta=[
        (('q_0','a','Z0','Z0'),[('q_0','AZ0','AZ0')]),
        (('q_0','a','A','A'),[('q_0','AA','AA')]),
        (('q_0','b','A','A'),[('q_1','epsilon','A')]),
        (('q_1','b','A','A'),[('q_1','epsilon','A')]),
        (('q_1','c','Z0','A'),[('q_2','Z0','epsilon')]),
        (('q_2','c','Z0','A'),[('q_2','Z0','epsilon')]),
        (('q_2','epsilon','Z0','Z0'),[('q_3','Z0','Z0')]),
     ]
)

def test_anbncn_pda():
    """Test if anbncn was corrected generated by TSPDA"""
    assert len(m.Q)==4
    assert len(m.sigma)==4
    assert len(m.gamma)==6
    assert m.q_0=="q_0"
    assert m.Z_0=="Z₀"
    assert len(m.A)==1
    assert len(m.ttable)==3

def test_anbncn_accepted():
    """Test valid strings for anbncn by TSPDA"""
    assert m.accepts("abc")
    assert m.accepts("aabbcc")
    assert m.accepts("aaabbbccc")
    assert m.accepts("aaaabbbbcccc")
    assert m.accepts("a"*100+"b"*100+"c"*100)

def test_anbncn_rejected():
    """Test invalid strings for anbncn by TSPDA"""
    assert m.accepts("")==False
    assert m.accepts("b")==False
    assert m.accepts("bc")==False
    assert m.accepts("c")==False
    assert m.accepts("abbb")==False
    assert m.accepts("abbbc")==False
    assert m.accepts("aababab")==False
    assert m.accepts("aabababcc")==False
    assert m.accepts("aaababaaabacc")==False
    assert m.accepts("baaababaaaabbaaaabaaaaaabaa")==False
    assert m.accepts("cbaaababaaaabbaaaabaaaaaabaa")==False

