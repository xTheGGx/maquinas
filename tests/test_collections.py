from nose.tools import *

from maquinas.collections import *

def test_jflap():
    """Test jflap examples"""
    assert len(jflap.list()) > 0
    first=jflap.list()[0]
    ex= jflap.get(first)
    assert len(ex)>0
    ex= jflap.examples[first]
    assert ex['content']
    assert ex['url']
    assert ex['type']=="jff"
    assert ex['url_source']
