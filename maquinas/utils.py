cyjs_style = [
    {
        "selector": "node",
        "style": {
            "border-color": "black",
            "background-color": "white",
            "border-width": 2,
            "label": "data(label)",
            "width": 50,
            "height": 50,
            "shape": "circle",
            "color": "black",
            "font-weight": 400,
            "text-halign": "center",
            "text-valign": "center",
            "font-size": 12,
        },
    },
    {
        "selector": ".final",
        "style": {
            "border-width": 5,
            "border-style": "double",
        },
    },
    {
        "selector": "edge",
        "style": {
            "width": 2,
            "curve-style": "bezier",
            "source-label": "data(label)",
            "line-color": "black",
            "text-halign": "center",
            "text-valign": "top",
            "target-arrow-color": "black",
            "target-arrow-shape": "triangle",
            "source-text-offset": 60,
            "source-text-margin-y": -10,
            "target-endpoint": "outside-to-node",
            "arrow-scale": 1,
        },
    },
]
