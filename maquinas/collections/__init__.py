import urllib


class Examples:
    """Class for examples"""

    def __init__(self):
        self.examples = {}

    def list(self):
        """List the names of the examples

        :returns: List of strings
        """
        return list(self.examples.keys())

    def add(
        self, name, type="maquinas", content=None, url=(None, None), description=None
    ):
        """Addes an example to the collection

        :param name: Name of the example
        :param type: Type of the example
        :param content: String of the example
        :param url: Tuple with url of the example, and url which describes the source
        :param descrption: String with descption
        :returns: The example dictionary
        """
        self.examples[name] = {
            "content": content,
            "type": type,
            "url": url[0],
            "url_source": url[1],
            "description": description,
        }
        return self.examples[name]

    def get(self, name):
        """Get the string content from an example

        :param name: Name of the example"""
        if (
            name in self.examples
            and "content" in self.examples[name]
            and "url" in self.examples[name]
            and not self.examples[name]["content"]
        ):
            webf = urllib.request.urlopen(self.examples[name]["url"])
            self.examples[name]["content"] = webf.read()
        if (
            name in self.examples
            and "content" in self.examples[name]
            and self.examples[name]["content"]
        ):
            return self.examples[name]["content"]

#: Examples from `JFLAP <https://www.jflap.org/>`_
jflap = Examples()

for name, url, url_source in [
    (
        "ex0.1a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex0.1a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.1a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.1a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.3a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.3a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.4a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.4a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.5a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.5a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.6a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.6a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.6b.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.6b.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.6c.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.6c.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.6d.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.6d.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.6e.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.6e.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.6f.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.6f.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.7a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.7a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.7b.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.7b.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.7c.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.7c.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.7d.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.7d.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.7e.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.7e.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex1.7f.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex1.7f.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2.1a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2.1a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2.1anew.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2.1anew.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2.2a-dup.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2.2a-dup.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2.2a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2.2a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2.3a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2.3a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2.3b.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2.3b.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2.3c.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2.3c.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2-dfa2mindfa-a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2-dfa2mindfa-a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2-dfa2mindfa-b.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2-dfa2mindfa-b.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2-dfa2mindfa-c.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2-dfa2mindfa-c.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2-dfa2mindfa-d.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2-dfa2mindfa-d.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2-dfa2mindfa-e.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2-dfa2mindfa-e.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2-dfa2mindfa-test.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2-dfa2mindfa-test.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2-dfa2nfa.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2-dfa2nfa.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2-nfa2dfa-a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2-nfa2dfa-a.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2-nfa2dfa-b.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2-nfa2dfa-b.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2-nfa2dfa-c.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2-nfa2dfa-c.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2-nfa2dfa-d.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2-nfa2dfa-d.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2-nfa2dfa-e.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2-nfa2dfa-e.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "ex2-nfa2dfa-f.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/ex2-nfa2dfa-f.jff",
        "https://www2.cs.duke.edu/csed/jflap/jflapbook/files/",
    ),
    (
        "faex.jff",
        "https://www.jflap.org/tutorial/fa/createfa/files/faex.jff",
        "https://www.jflap.org/tutorial/fa/createfa/fa.html#deleting",
    ),
    (
        "ex1.3a_alt.jff",
        "https://www.jflap.org/tutorial/fa/createfa/files/ex1.3a.jff",
        "https://www.jflap.org/tutorial/fa/createfa/fa.html#nfa",
    ),
    (
        "ADD_TRAP.jff",
        "https://www.jflap.org/tutorial/fa/trapstate/ADD_TRAP.jff",
        "https://www.jflap.org/tutorial/fa/trapstate/index.html#Add%20a%20Trap%20State%20to%20DFA|outline",
    ),
    (
        "nfaToDfa.jff,",
        "https://www.jflap.org/tutorial/fa/nfa2dfa/nfaToDfa.jff",
        "https://www.jflap.org/tutorial/fa/nfa2dfa/index.html#introduction",
    ),
    (
        "nfaToDfa.jff,",
        "https://www.jflap.org/tutorial/fa/dfa2mindfa/dfaToMinDFA.jff",
        "https://www.jflap.org/tutorial/fa/dfa2mindfa/index.html#introduction",
    ),
]:
    jflap.add(name, type="jff", url=(url, url_source))
